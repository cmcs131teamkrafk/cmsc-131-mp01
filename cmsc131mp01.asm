; macros
;+++++++++++++++++++++

	;/**
	; *	display string on console
	; *	@param string, string length
	; **/
	%macro print 2
		mov eax, 4
		mov ebx, 1
		mov ecx, %1		;1st param = string
		mov edx, %2		;2nd param = string length
		int 0x80
	%endmacro

	;/**
	; *	prompt user for input
	; *	@param input variable
	; **/
	%macro prompt 1
		mov eax, 3
		mov ebx, 2
		mov ecx, %1		;where input is to be stored
		int 0x80
	%endmacro

;+++++++++++++++++++++

section .data
	input_msg db "Input (e to exit): "
	input_msg_len equ $-input_msg
	ty_msg db "Thanks for using. Bye!"
	ty_msg_len equ $-ty_msg
	err_msg db 'Math Error'
	err_msg_len equ $- err_msg
	new_line db 10
	exp_arr times 100 dd 0
	postfix_arr times 100 dd 0
section .bss
	stack_size resb 1	; stack size
	exp	resb 100		; expression
	res	resb 10		; result
	temp resd 1
	temp1 resb 1
	place_ten resd 1
section .text
	global _start

_start:

;+++++++++++++++++++++++++++++++++++++++++++
		while_not_exit:
;//////////////////////////////////////////////////////	
	call clear_procedure

	; input expression
	;##################
	print input_msg, input_msg_len
	;print res, 100					display the res each iteration
	prompt exp
	;##################
	
	; check if input is 'e'
	;##################
	mov al, byte[exp]
	cmp al, 'e'
	je exit
	;##################
	
	; tokenize user input
	;##################
		mov ecx, -1
	mov esi, -1
	mov edi, 0
	mov eax, 1
	mov [place_ten], eax				; !!!! You can comment out the tokenize block to see the previous way of calculation!!!!
	mov eax, 0
	mov [temp], eax

	tokenize_loop:
		xor dl, dl
		inc ecx
		inc esi
		mov dl, [exp+ecx]
		cmp edx, 0
		je convert_to_postfix
		cmp edx, '0'
		jge append
		
		xor ebx, ebx
		mov ebx, [temp]
		mov [exp_arr+edi*4], ebx
		
		inc edi
		mov [temp1], dl
		xor edx, edx
		xor eax, eax
		mov al, [temp1]
		sub al, '0'							; convert operator to dec
		mov [exp_arr+edi*4], eax
		inc edi
		mov esi, -1
		
		mov al, 1
		mov [place_ten], al
		mov al, 0
		mov [temp], al

		jmp tokenize_loop
		
			append:
		sub dl, '0'
		mov eax, [temp]
		mov ebx, [place_ten]
		mul bl
		add al, dl
		mov [temp], eax
		cmp ebx, 10
		je jmp_tokenize_loop
		call inc_place_ten
			jmp_tokenize_loop:
		jmp tokenize_loop
	;##################
	
	; convert to postfix notation
	;##################
		convert_to_postfix:
	mov eax, 0
	mov [stack_size], al
	mov esi, 0
	mov edi, 0
	conversion_loop:

		; extract each character from input expression
		xor eax, eax
		mov eax, [exp_arr+esi*4]		; opThis
		
		; compare if end of expression
		cmp al, 0
		je init_loop_pop
		
	;|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		; check if operator
		cmp al, 0
		jl is_operator
		; it is an operand
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		;sub eax, '0'
		mov [postfix_arr+edi*4], eax
		;add eax, '0'
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		inc edi
		inc esi
		jmp conversion_loop
		
			is_operator:
		;sub al, '0'
			repeat_for_star:

		; if stack size = 0
		mov bl, [stack_size]
		cmp bl, 0
		je push_al
		; stack size != 0
		mov cl, [stack_size]
		call dec_stack_size
		mov [stack_size], cl
		pop ebx
		
		add al, '0'
		add bl, '0'
		call assign_precedence_lvl
		sub al, '0'
		sub bl, '0'
		; compare precedence of al (opThis) and bl (opTop)
		cmp cl, dl
		jg push_opTop
		; opThis <= opTop --> star
		add bl, '0'
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		sub ebx, '0'
		mov [postfix_arr+edi*4], ebx
		add ebx, '0'
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		inc edi
		jmp repeat_for_star
		
			push_opTop:
		mov cl, [stack_size]
		call inc_stack_size
		mov [stack_size], cl
		push ebx
		jmp push_al
		
			push_al:
		mov cl, [stack_size]
		call inc_stack_size
		mov [stack_size], cl
		push eax
	;|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	
			end_of_conversion_loop:
		inc esi
	jmp conversion_loop

		init_loop_pop:
	pop ecx
	mov cl, [stack_size]
	call dec_stack_size
	mov [stack_size], cl
		loop_pop:
	mov cl, [stack_size]
	cmp cl, 0
	je display
	call dec_stack_size
	mov [stack_size], cl
	pop ecx
	add cl, '0'
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	sub ecx, '0'
	mov [postfix_arr+edi*4], ecx
	add ecx, '0'
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	inc edi
	jmp loop_pop
	;##################

		display:
;//////////////////////////////////////////////////////

	mov esi, postfix_arr
	dec edi
	call print_array

	mov eax, 4
	mov ebx, 1
	mov ecx, new_line
	mov edx, 1
	int 0x80
	
	; evaluate postfix notation
	;##################
	evaluation_loop:
		;array han postfix expression
		
		mov ebx, postfix_arr
		mov ecx, -1
	
		evaluate:
			inc ecx
			mov eax, [postfix_arr + ecx * 4]
			cmp eax, 0H ;tanggal end of line code
			je check_end_string
			
			check_operator:
			add eax, '0'
			
			cmp eax, '+'
			je addition
		
			cmp eax, '-'
			je subtraction
		
			cmp eax, '*'
			je multiplication
		
			cmp eax, '/'
			je division
		
			sub eax, '0'
			jmp push_op

		check_end_string:
			mov edx, 5 ;use the variable for array size in replacement with the constant 5
			mov [temp], ecx
			mov edi, [temp]
			sub edi, 1
			
			check_end:
			inc edi
			mov ebx, [postfix_arr+edi*4]
			cmp ebx, 0H
			je end
			jg check_operator
			jl check_operator
			
			end:
			cmp edx, edi
			je done
			jmp check_end
			
		push_op:
			xor edx, edx
			push eax
			jmp evaluate
	
		addition:
			xor edx, edx
			xor ebx, ebx
			pop ebx
			pop eax
			add eax, ebx
			push eax
			jmp evaluate
	
		subtraction:
			xor edx, edx
			xor ebx, ebx
			pop ebx
			pop eax
			sub eax, ebx
			push eax
			jmp evaluate

		multiplication:
			xor edx, edx
			xor ebx, ebx
			pop ebx
			pop eax
			mul ebx
			push eax
			jmp evaluate

		division:
			xor edx, edx
			xor ebx, ebx
			pop ebx
			cmp ebx, 0
			jne continue
			call print_err
			jmp while_not_exit
		continue:
			pop eax
			div ebx
			push eax
			jmp evaluate
	
		done:
			pop eax
			cmp eax, 0
			jg digits
			mov ebx, -1
			mul ebx
			mov ecx, 9
			mov [temp], byte '-'
		
		digits:
			;cmp eax, 0
			;je exit
			mov edx, 0
			mov ebx, 10
			div ebx
			or edx, 30h
			mov [res + ecx], dl
			dec ecx
			cmp eax, 0
			jne digits
			
	; display result
	mov eax, 4
	mov ebx, 1
	mov ecx, res
	mov edx, 10
	int 0x80
	
	mov eax, 4
	mov ebx, 1
	mov ecx, new_line
	mov edx, 1
	int 0x80
	;##################
	jmp while_not_exit
	
	print_err:
		mov eax, 4
		mov ebx, 1
		mov ecx, err_msg
		mov edx, err_msg_len
		int 80h
		ret
;+++++++++++++++++++++++++++++++++++++++++++

		exit:
	mov eax, 4
	mov ebx, 1
	mov ecx, ty_msg
	mov edx, ty_msg_len
	int 0x80
	
	mov eax, 4
	mov ebx, 1
	mov ecx, new_line
	mov edx, 1
	int 0x80
		
	mov eax, 1
	int 0x80
	
	print_array:
		mov edx, 1
		mov ecx, [esi]
		add ecx, '0'
		push ecx
		mov ecx, esp
		mov ebx, 1
		mov eax, 4
		int 0x80
		pop ecx
		add esi, 4
		dec edi
		jns print_array
	ret
	
		assign_precedence_lvl:
	; add precedence level for opThis
			check_plus_cl:
		mov dl, al
		cmp dl, '+'
		jne check_minus_cl
		mov cl, 0				; precedence level = 0
		jmp compare_to_opTop
		
			check_minus_cl:
		mov dl, al
		cmp dl, '-'
		jne check_asterisk_cl
		mov cl, 0				; precedence level = 0
		jmp compare_to_opTop
		
			check_asterisk_cl:
		mov dl, al
		cmp dl, '*'
		jne check_divide_cl
		mov cl, 1				; precedence level = 1
		jmp compare_to_opTop
		
			check_divide_cl:
		mov cl, 1
		
		; add precedence level for opTop
			compare_to_opTop:
		mov dl, bl
		cmp dl, '+'
		jne check_minus_dl
		mov dl, 0				; precedence level = 0
		jmp end_comparison
		
			check_minus_dl:
		mov dl, bl
		cmp dl, '-'
		jne check_asterisk_dl
		mov dl, 0				; precedence level = 0
		jmp end_comparison
		
			check_asterisk_dl:
		mov dl, bl
		cmp dl, '*'
		jne check_divide_dl
		mov dl, 1				; precedence level = 1
		jmp end_comparison
		
			check_divide_dl:
		mov dl, 1
			end_comparison:
	ret
	
		inc_stack_size:
	mov cl, [stack_size]
	inc cl
	mov [stack_size], cl
	ret
	
		dec_stack_size:
	mov cl, [stack_size]
	dec cl
	mov [stack_size], cl
	ret
	
		inc_place_ten:
	mov al, [place_ten]
	mov bl, 10
	mul bl
	mov [place_ten], al
	ret
	
		clear_procedure:
	mov ecx, 10

		clear1:
	mov al, 0
	mov [temp], al
	mov [temp1], al
	;mov [temp+ecx-1], byte '1'
	;loop clear1
	;mov [temp1+ecx], dword 0
	;mov [temp2+ecx], dword 0
	
	mov ecx, 100
		clear2:
	mov [exp_arr+(ecx-1)*4], dword 0
	loop clear2
	
	mov ecx, 10
		clear3:
	mov [res+ecx-1], byte 0
	loop clear3
	
	mov ecx, 100
		clear4:
	mov [exp + ecx-1], byte 0
	loop clear4
	
	mov ecx, 100
	
		clear5:
	mov [postfix_arr+(ecx-1)*4], dword  0
	loop clear5
	ret